//console.log('Hello world')
//3. 

let studentName ="";
let studentArray = [];
function addStudent(studentName){
  studentArray.push(studentName);
  console.log(studentName + " was added in the array.");
  console.log(studentArray);
}


//4. Create a countStudents() function that will print the total number of students in the array.

function countStudents(){
	console.log("There are a total of " + studentArray.length + " students in the array.");
}

//5. Create a printStudents() function that will sort and individually print the students (sort and forEach methods) in the array.


function printStudents(){

  console.log(studentArray.sort());

  studentArray.forEach(function(student){
  console.log(student);
})
}


//6. Create a findStudent() function that will do the following:
// Search for a student name when a keyword is given (filter method).
// If one match is found print the message studentName is an enrollee.
// If multiple matches are found print the message studentNames are enrollees.
// If no match is found print the message studentName is not an enrollee.
// The keyword given should not be case sensitive.


function findStudent(studentName){

  let filterStudents = studentArray.filter(function(student){
   
    return student.toLowerCase().includes(studentName);
  })
  
  if(filterStudents.length === 1){
    console.log(filterStudents + " is an enrollee.");
  }
  else if(filterStudents.length >= 2){
    console.log(filterStudents + " are enrollees.");
  }
  else{
    console.log("No match is found. " + studentName + " is not an enrollee.");
  }
}

